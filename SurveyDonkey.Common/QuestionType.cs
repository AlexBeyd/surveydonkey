﻿namespace SurveyDonkey.Common
{
    public enum QuestionType
    {
        SingleChoice,
        MultiChoice,
        FreeText,
        Scale
    }
}