﻿using SurveyDonkey.API.Mappers;
using SurveyDonkey.API.Models;
using SurveyDonkey.DB.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace SurveyDonkey.API.Controllers
{
    public class SurveyStatsController : SurveyBaseController
    {
        public SurveyStatsController(ISurveyRepository repo, ISurveyMapper mapper) : base(repo, mapper)
        {
        }

        /// <summary>
        /// Returns collection of a survey statistics data
        /// </summary>
        /// <param name="id">Survey Template Id, Optional. Default: 1</param>
        /// <param name="from">From date filter. NOT IMPLEMENTED</param>
        /// <param name="to">To date filter. NOT IMPLEMENTED</param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/v1/surveystats/{id:int?}")]
        public async Task<HttpResponseMessage> GetSurveyStats(int id = 1, [FromUri] DateTime? from = null, [FromUri] DateTime? to = null)
        {
            SurveyStats result = _mapper.ToStatsDto(await _surveyRepo.GetSurveyResultsAsync(id, from, to)); 

            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

    }
}
