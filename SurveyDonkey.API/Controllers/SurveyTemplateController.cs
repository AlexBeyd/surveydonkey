﻿using SurveyDonkey.API.Mappers;
using SurveyDonkey.API.Models;
using SurveyDonkey.Common.Exceptions;
using SurveyDonkey.DB.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace SurveyDonkey.API.Controllers
{
    [EnableCors(origins: "http://localhost:16335", headers: "*", methods: "*")]
    public class SurveyTemplateController : SurveyBaseController
    {
        public SurveyTemplateController(ISurveyRepository repo, ISurveyMapper mapper) : base(repo, mapper)
        {
        }

        [HttpGet]
        [Route("api/v1/surveytemplate/{Id}")]
        public async Task<HttpResponseMessage> GetSurveyTemplateAsync(int Id)

        {
            SurveyTemplate result = null;

            try
            {
                result = _mapper.ToDto(await _surveyRepo.GetSurveyTemplate(Id));
            }
            catch (EntityNotFoundException)
            {
                return Request.CreateResponse(HttpStatusCode.NoContent);
            }

            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        [HttpGet]
        [Route("api/v1/surveytemplates")]
        public async Task<HttpResponseMessage> GetAllSurveyTemplatesAsync()
        {
            List<SurveyTemplate> result = (await _surveyRepo.GetSurveyTemplatesList()).Select(s => _mapper.ToDto(s)).ToList();

            return Request.CreateResponse(HttpStatusCode.OK, result);
        }        
    }
}
