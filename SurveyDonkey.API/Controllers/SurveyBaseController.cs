﻿using SurveyDonkey.API.Mappers;
using SurveyDonkey.DB.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SurveyDonkey.API.Controllers
{
    public abstract class SurveyBaseController : ApiController
    {
        protected ISurveyRepository _surveyRepo;
        protected ISurveyMapper _mapper;

        public SurveyBaseController(ISurveyRepository repo, ISurveyMapper mapper)
        {
            _surveyRepo = repo;
            _mapper = mapper;
        }
    }
}
