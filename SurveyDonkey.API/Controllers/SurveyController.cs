﻿using SurveyDonkey.API.Mappers;
using SurveyDonkey.API.Models;
using SurveyDonkey.DB.Interface;
using System.Threading.Tasks;
using System.Web.Http;

namespace SurveyDonkey.API.Controllers
{
    public class SurveyController : SurveyBaseController
    {
        public SurveyController(ISurveyRepository repo, ISurveyMapper mapper) : base(repo, mapper)
        {
        }

        [HttpPost]
        [Route("api/v1/survey")]
        public async Task CreateNewSurvey([FromBody] Survey survey)
        {
            await _surveyRepo.CreateSurveyAsync(_mapper.ToEntity(survey));
        }        
    }
}

