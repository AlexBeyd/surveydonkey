﻿using SurveyDonkey.API.Models;
using SurveyDonkey.Common;
using SurveyDonkey.DB.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SurveyDonkey.API.Mappers
{
    [Injectable]
    public class SurveyApiMapper : ISurveyMapper
    {
        public SurveyEntity ToEntity(Survey survey)
        {
            return new SurveyEntity
            {
                SurveyTemplateId = survey.SurveyTemplateId,
                CreatedTimeStamp = DateTime.Now,
                Id = Guid.NewGuid(),
                Responses = survey.Responses.Select(ToEntity).ToList()
            };
        }

        public ResponseEntity ToEntity(Response response)
        {
            return new ResponseEntity
            {
                ResponseTemplateId = response.ResponseTemplateId,
                QuestionTemplateId = response.QuestionTemplateId,
                Contents = response.Contents
            };
        }


        public QuestionTemplate ToDto(QuestionTemplateEntity questionEntity)
        {
            var result = new QuestionTemplate
            {
                ResponsesList = questionEntity.ResponseTemplatesList.Select(ToDto).ToList(),
                QuestionType = questionEntity.QuestionType,
                Content = questionEntity.Content
            };

            //add question template id for easier statistics create
            foreach (var response in result.ResponsesList)
            {
                response.QuestionTemplateId = questionEntity.Id;
            }

            return result;
        }

        public ResponseTemplate ToDto(ResponseTemplateEntity responseEntity)
        {
            return new ResponseTemplate
            {
                Contents = responseEntity.Content,
                TemplateId = responseEntity.Id,
                IsSelected = false
            };
        }

        public Response ToDto(ResponseEntity responseEntity)
        {
            return new Response
            {
                Contents = string.IsNullOrWhiteSpace(responseEntity.Contents) ? responseEntity.ResponseTemplate.Content : responseEntity.Contents
            };
        }

        public Survey ToDto(SurveyEntity surveyEntity)
        {
            return new Survey
            {
                CreatedTimeStamp = surveyEntity.CreatedTimeStamp,
                Responses = surveyEntity.Responses.Select(ToDto).ToList()
            };
        }

        public SurveyTemplate ToDto(SurveyTemplateEntity surveyEntity)
        {
            return new SurveyTemplate
            {
                SurveyName = surveyEntity.SurveyName,
                CreatedTimestamp = surveyEntity.CreatedTimestamp,
                QuestionsList = surveyEntity.QuestionTemplatesList.Select(ToDto).ToList(),
                SurveyTemplateId = surveyEntity.Id
            };
        }

        public SurveyStats ToStatsDto(SurveyStatsEntity surveyStatsEntity)
        {
            return new SurveyStats
            {
                QuestionStats = surveyStatsEntity.QuestionStats.Select(ToStatsDto).ToList()
            };
        }

        public QuestionStats ToStatsDto(QuestionStatsEntity questionStatsEntity)
        {
            return new QuestionStats
            {
                Contents = questionStatsEntity.Contents,
                QuestionTypeId = questionStatsEntity.QuestionTypeId,
                ResponsesStats = ToStatsDto(questionStatsEntity.ResponsesStats)
            };
        }

        public ResponseStats ToStatsDto(ResponseStatsEntity responseStatsEntity)
        {
            return new ResponseStats
            {
                ResponsesStats = responseStatsEntity.ResponsesStats
            };
        }
    }
}