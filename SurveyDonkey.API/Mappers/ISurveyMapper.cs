﻿using SurveyDonkey.API.Models;
using SurveyDonkey.DB.Models;
using System.Collections.Generic;

namespace SurveyDonkey.API.Mappers
{
    public interface ISurveyMapper
    {
        #region Persist New Responded Survey
        SurveyEntity ToEntity(Survey surveyDto);
        ResponseEntity ToEntity(Response response);
        #endregion

        #region Display Template Survey
        SurveyTemplate ToDto(SurveyTemplateEntity surveyEntity);
        QuestionTemplate ToDto(QuestionTemplateEntity questionEntity);
        ResponseTemplate ToDto(ResponseTemplateEntity answerDto);
        #endregion

        #region Future usage to display responded survey
        Survey ToDto(SurveyEntity surveyEntity);
        Response ToDto(ResponseEntity responseEntity);
        #endregion

        #region Statistics
        SurveyStats ToStatsDto(SurveyStatsEntity surveyEntity);
        ResponseStats ToStatsDto(ResponseStatsEntity responseStatsEntity);
        #endregion
    }
}
