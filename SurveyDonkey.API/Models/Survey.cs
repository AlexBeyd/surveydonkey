﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SurveyDonkey.API.Models
{
    /// <summary>
    /// Object received from Survey Page containing actual users' response for the survey generated from survey template
    /// </summary>
    public class Survey
    {
        /// <summary>
        /// Id of original survey tempate
        /// </summary>
        public int SurveyTemplateId { get; set; }
        /// <summary>
        /// Date/Time this survey response was created
        /// </summary>
        public DateTime CreatedTimeStamp { get; set; }
        /// <summary>
        /// List of responses
        /// </summary>
        public virtual ICollection<Response> Responses { get; set; }
    }
}