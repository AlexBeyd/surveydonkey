﻿namespace SurveyDonkey.API.Models
{
    public class ResponseTemplate
    {
        public string Contents { get; set; }
        public bool IsSelected { get; set; }
        public int TemplateId { get; set; }
        public int QuestionTemplateId { get; set; }
    }
}