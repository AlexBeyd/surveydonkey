﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SurveyDonkey.API.Models
{
    public class SurveyStats
    {
        public List<QuestionStats> QuestionStats { get; set; }
    }
}