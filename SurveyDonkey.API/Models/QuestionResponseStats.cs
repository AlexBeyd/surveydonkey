﻿using System.Collections.Generic;

namespace SurveyDonkey.API.Models
{
    public class QuestionResponseStats
    {
        public string Question { get; set; }
        public Dictionary<string, int> ResponsesSummary { get; set; }
    }
}