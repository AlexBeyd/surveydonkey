﻿namespace SurveyDonkey.API.Models
{
    public class Response
    {
        public int ResponseTemplateId { get; set; }

        /// <summary>
        /// Used for free text responses
        /// </summary>
        public string Contents { get; set; }
        public int QuestionTemplateId { get; set; }
    }
}