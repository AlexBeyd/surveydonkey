﻿using System.Collections.Generic;

namespace SurveyDonkey.API.Models
{
    public class ResponseStats
    {
        public Dictionary<string, int> ResponsesStats { get; set; }
    }
}