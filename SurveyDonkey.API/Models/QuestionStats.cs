﻿using SurveyDonkey.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SurveyDonkey.API.Models
{
    public class QuestionStats
    {
        public  QuestionType QuestionTypeId { get; set; }
        public string Contents { get; set; }
        public ResponseStats ResponsesStats { get; set; }

    }
}