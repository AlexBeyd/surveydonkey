﻿using System;
using System.Collections.Generic;

namespace SurveyDonkey.API.Models
{
    public class SurveyTemplate
    {
        public int SurveyTemplateId { get; set; }
        public string SurveyName { get; set; }
        public DateTime CreatedTimestamp { get; set; }
        public List<QuestionTemplate> QuestionsList { get; set; }        
    }
}