﻿using SurveyDonkey.Common;
using System.Collections.Generic;

namespace SurveyDonkey.API.Models
{
    public class QuestionTemplate
    {
        public string Content  { get; set; }
        public List<ResponseTemplate> ResponsesList { get; set; }
        public QuestionType QuestionType { get; set; }  
    }
}