﻿using Autofac;
using Autofac.Integration.WebApi;
using SurveyDonkey.Common;
using System;
using System.Reflection;
using System.Web.Http;

namespace SurveyDonkey.API
{
    public static class AutofacDependencyBuilder
    {
        public static void DependencyBuilder()
        {
            var builder = new ContainerBuilder();

            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            builder.RegisterAssemblyTypes(AppDomain.CurrentDomain.GetAssemblies()).
                Where(t => t.GetCustomAttribute<Injectable>() != null).
                AsImplementedInterfaces().InstancePerRequest();

            var container = builder.Build();

            var resolver = new AutofacWebApiDependencyResolver(container);

            GlobalConfiguration.Configuration.DependencyResolver = resolver;
        }
    }
}