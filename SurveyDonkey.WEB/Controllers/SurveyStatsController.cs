﻿using Newtonsoft.Json;
using SurveyDonkey.API.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Web.Mvc;
using System.Web.Routing;

namespace SurveyDonkey.WEB.Controllers
{
    

    public class SurveyStatsController : Controller
    {
        private string baseUrl = "http://localhost:5471/api/v1/";

        private SurveyStats surveyTemplateObject = null;

        // GET: SurveyStats
        public ActionResult Index()
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseUrl);

                //HTTP GET
                var result = client.GetAsync("surveystats").Result.Content.ReadAsStringAsync().Result;

                surveyTemplateObject = JsonConvert.DeserializeObject<SurveyStats>(result);
            }

            //by default always use first template
            return View(surveyTemplateObject);
        }

        public ActionResult DrawChart(string jsonValues)
        {
            if (jsonValues == null)
            {
                return View();
            }

            Dictionary<string, int> values = new Dictionary<string, int>();
            foreach (var item in JsonConvert.DeserializeObject<Dictionary<string, int>>(jsonValues))
            {
                values.Add(item.Key, item.Value);
            } 

            return View(values);
        }
    }
}