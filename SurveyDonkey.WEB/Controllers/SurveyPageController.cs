﻿using Newtonsoft.Json;
using SurveyDonkey.API.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Mvc;

namespace SurveyDonkey.WEB.Controllers
{
    public class SurveyPageController : Controller
    {
        private string baseUrl = "http://localhost:5471/api/v1/";
        // GET: SurveyPage
        [HttpGet]
        public ActionResult Index()
        {
            List<SurveyTemplate> surveyTemplateObject = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseUrl);

                //HTTP GET
                var result = client.GetAsync("surveytemplates").Result.Content.ReadAsStringAsync().Result;

                surveyTemplateObject = JsonConvert.DeserializeObject<List<SurveyTemplate>>(result);
            }

            //by default always use first template
            return View(surveyTemplateObject[0]);
        }

        // POST: SurveyPage/Create
        [System.Web.Http.HttpPost]
        public ActionResult Create(SurveyTemplate myNewSurvey)
        {
            try
            {
                var responsesList = new List<Response>();
                foreach (var question in myNewSurvey.QuestionsList)
                {
                    foreach (var response in question.ResponsesList)
                    {
                        if (response.IsSelected)
                        {
                            responsesList.Add(new Response
                            {
                                Contents = response.Contents,
                                ResponseTemplateId = response.TemplateId,
                                QuestionTemplateId = response.QuestionTemplateId
                            });
                        }
                    }
                }

                var respondedSurvey = new Survey
                {
                    CreatedTimeStamp = DateTime.Now,
                    SurveyTemplateId = myNewSurvey.SurveyTemplateId,
                    Responses = responsesList
                };

                //post survey
                //*************************************************************
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(baseUrl);

                    //HTTP POST
                    var content = new ByteArrayContent(System.Text.Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(respondedSurvey)));
                    content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                    var result = client.PostAsync("survey", content).Result;
                }

                //rerturn to brand new survey
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
