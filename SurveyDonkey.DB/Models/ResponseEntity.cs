﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace SurveyDonkey.DB.Models
{
    [Table("Responses")]
    public class ResponseEntity : IdentifiableEntity
    {
        [NotMapped]
        public int ResponseTemplateId { get; set; }
        public int QuestionTemplateId { get; set; }
        public ResponseTemplateEntity ResponseTemplate { get; set; }
        public SurveyEntity Survey { get; set; }
        public string Contents { get; set; }
    }
}
