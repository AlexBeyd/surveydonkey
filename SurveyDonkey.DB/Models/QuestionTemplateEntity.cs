﻿using SurveyDonkey.Common;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace SurveyDonkey.DB.Models
{
    /// <summary>
    /// Question object to be stored inside repository
    /// </summary>
    [Table("QuestionsTemplates")]
    public class QuestionTemplateEntity : IdentifiableEntity
    {
        public string Content { get; set; }        
        public QuestionType QuestionType { get; set; }
        public virtual ICollection<SurveyTemplateEntity> SurveyTemplatesList { get; set; }
        public virtual ICollection<ResponseTemplateEntity> ResponseTemplatesList { get; set; }
    }
}
