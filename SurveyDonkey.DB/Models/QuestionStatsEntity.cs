﻿using SurveyDonkey.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SurveyDonkey.DB.Models
{
    public class QuestionStatsEntity
    {
        public QuestionType QuestionTypeId { get; set; }
        public string Contents { get; set; }
        public ResponseStatsEntity ResponsesStats { get; set; }
    }
}
