﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace SurveyDonkey.DB.Models
{
    [Table("Surveys")]
    public class SurveyEntity
    {
        public int SurveyTemplateId { get; set; }
        public Guid Id { get; set; }
        public DateTime CreatedTimeStamp { get; set; }
        public virtual ICollection<ResponseEntity> Responses { get; set; }
    }
}
