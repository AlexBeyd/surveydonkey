﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace SurveyDonkey.DB.Models
{
    /// <summary>
    /// Survey object to fetch from repository
    /// </summary>

    [Table("SurveysTemplates")]
    public class SurveyTemplateEntity : IdentifiableEntity
    {
        public string SurveyName { get; set; }
        public DateTime CreatedTimestamp { get; set; }
        public virtual ICollection<QuestionTemplateEntity> QuestionTemplatesList { get; set; }
    }
}