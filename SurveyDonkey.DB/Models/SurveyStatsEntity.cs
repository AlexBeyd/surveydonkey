﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SurveyDonkey.DB.Models
{
    public class SurveyStatsEntity
    {
        public List<QuestionStatsEntity> QuestionStats { get; set; }
    }
}
