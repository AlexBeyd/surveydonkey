﻿namespace SurveyDonkey.DB.Models
{
    public class IdentifiableEntity
    {
        public int Id { get; set; }
    }
}
