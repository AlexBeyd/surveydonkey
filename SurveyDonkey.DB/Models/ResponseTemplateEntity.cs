﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace SurveyDonkey.DB.Models
{
    [Table("ResponsesTemplates")]
    public class ResponseTemplateEntity : IdentifiableEntity
    {
        public string Content { get; set; }
        public virtual ICollection<QuestionTemplateEntity> QuestionTemplatesList { get; set; }
        public virtual ICollection<ResponseEntity> ResponsesList { get; set; }
    }
}
