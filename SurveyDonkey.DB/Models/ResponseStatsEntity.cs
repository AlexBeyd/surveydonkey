﻿using System.Collections.Generic;

namespace SurveyDonkey.DB.Models
{
    public class ResponseStatsEntity
    {
        public Dictionary<string, int> ResponsesStats { get; set; }
    }
}