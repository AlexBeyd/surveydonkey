﻿using SurveyDonkey.DB.Models;
using System.Data.Entity;

namespace SurveyDonkey.DB.Context
{
    public class SurveyContext : DbContext
    {
        public SurveyContext() : base("SurveyDonkey") {
        }

        public DbSet<SurveyTemplateEntity> SurveyTemplates { get; set; }
        public DbSet<QuestionTemplateEntity> SurveyQuestionTemplates { get; set; }
        public DbSet<ResponseTemplateEntity> SurveyResponseTemplates { get; set; }
        public DbSet<SurveyEntity> Surveys { get; set; }
        public DbSet<ResponseEntity> Responses { get; set; }
    }
}
