﻿using SurveyDonkey.DB.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SurveyDonkey.DB.Interface
{
    public interface ISurveyRepository
    {
        Task<SurveyTemplateEntity> GetSurveyTemplate(int Id);
        Task CreateSurveyAsync(SurveyEntity surveyEntity);
        Task<List<SurveyTemplateEntity>> GetSurveyTemplatesList();
        Task<SurveyStatsEntity> GetSurveyResultsAsync(int id, DateTime? from, DateTime? to);
    }
}