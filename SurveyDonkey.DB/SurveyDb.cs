﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using SurveyDonkey.Common;
using SurveyDonkey.Common.Exceptions;
using SurveyDonkey.DB.Context;
using SurveyDonkey.DB.Interface;
using SurveyDonkey.DB.Models;

namespace SurveyDonkey.DB
{
    [Injectable]
    public class SurveyDb : ISurveyRepository
    {
        private SurveyContext _context;

        public SurveyDb()
        {
            _context = new SurveyContext();
        }

        public async Task<SurveyTemplateEntity> GetSurveyTemplate(int Id)
        {
            var result = await _context.SurveyTemplates.FindAsync(Id);

            if (result == null)
            {
                throw new EntityNotFoundException();
            }

            return result;
        }

        public async Task<List<SurveyTemplateEntity>> GetSurveyTemplatesList()
        {
            var result = await _context.SurveyTemplates.Include(st => st.QuestionTemplatesList).ToListAsync();

            if (result == null)
            {
                throw new EntityNotFoundException();
            }

            return result;
        }

        public async Task CreateSurveyAsync(SurveyEntity surveyEntity)
        {
            var updatedResponses = new List<ResponseEntity>();

            foreach (var response in surveyEntity.Responses)
            {
                response.ResponseTemplate = _context.SurveyResponseTemplates.FirstOrDefault(r => r.Id == response.ResponseTemplateId);
                updatedResponses.Add(response);
            }

            surveyEntity.Responses = updatedResponses;
            _context.Surveys.Add(surveyEntity);
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Note: Dates range is not implemented
        /// </summary>
        /// <param name="from">Not implemented - from date</param>
        /// <param name="to">Not implemented - to date</param>
        /// <returns></returns>
        public async Task<SurveyStatsEntity> GetSurveyResultsAsync(int id, DateTime? from, DateTime? to)
        {
            SurveyStatsEntity stats = new SurveyStatsEntity
            {
                QuestionStats = new List<QuestionStatsEntity>()
            };

            List<SurveyEntity> surveyList =
                await _context.Surveys.Include(s => s.Responses.Select(r => r.ResponseTemplate)).Where(s => s.SurveyTemplateId == id).ToListAsync();

            foreach (var survey in surveyList)
            {
                SurveyTemplateEntity surveyTemplate =
                    _context.SurveyTemplates.Include(st => st.QuestionTemplatesList).FirstOrDefault(st => st.Id == survey.SurveyTemplateId);

                foreach (var response in survey.Responses)
                {
                    ResponseTemplateEntity responseTemplate = response.ResponseTemplate;

                    //special case for free text responses where the actual response is recorded directly 
                    //into Responses table instead of being part of response template
                    responseTemplate.Content = !string.IsNullOrWhiteSpace(response.Contents) ? response.Contents : responseTemplate.Content;

                    QuestionTemplateEntity questionTemplate = surveyTemplate.QuestionTemplatesList.FirstOrDefault(qt => qt.Id == response.QuestionTemplateId);

                    string question = questionTemplate.Content;

                    if (!stats.QuestionStats.Any(qs => qs.Contents.Equals(question)))
                    {
                        //no such question exists, create new and add the response to stats
                        stats.QuestionStats.Add(
                            new QuestionStatsEntity
                            {
                                Contents = question,
                                QuestionTypeId = questionTemplate.QuestionType,
                                ResponsesStats = new ResponseStatsEntity
                                {
                                    ResponsesStats = new Dictionary<string, int> { { responseTemplate.Content, 1 } }
                                }
                            });
                    }
                    else
                    {
                        //the question already exists
                        var questionStats = stats.QuestionStats.FirstOrDefault(qs=>qs.Contents.Equals(question));
                        if (questionStats.ResponsesStats == null || !questionStats.ResponsesStats.ResponsesStats.ContainsKey(responseTemplate.Content))
                        {
                            if (questionStats.ResponsesStats == null)
                            {
                                questionStats.ResponsesStats.ResponsesStats = new Dictionary<string, int>();
                            }

                            //no such response to the question exist yet
                            questionStats.ResponsesStats.ResponsesStats.Add(responseTemplate.Content, 1);
                        }
                        else
                        {
                            //the same response is found, increment by one
                            questionStats.ResponsesStats.ResponsesStats[responseTemplate.Content]++;
                        }
                    }
                }
            }

            return stats;
        }
    }
}
