namespace SurveyDonkey.DB.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitalMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Responses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        QuestionTemplateId = c.Int(nullable: false),
                        Contents = c.String(),
                        ResponseTemplate_Id = c.Int(),
                        Survey_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ResponsesTemplates", t => t.ResponseTemplate_Id)
                .ForeignKey("dbo.Surveys", t => t.Survey_Id)
                .Index(t => t.ResponseTemplate_Id)
                .Index(t => t.Survey_Id);
            
            CreateTable(
                "dbo.ResponsesTemplates",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Content = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.QuestionsTemplates",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Content = c.String(),
                        QuestionType = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SurveysTemplates",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SurveyName = c.String(),
                        CreatedTimestamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Surveys",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        SurveyTemplateId = c.Int(nullable: false),
                        CreatedTimeStamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.QuestionTemplateEntityResponseTemplateEntities",
                c => new
                    {
                        QuestionTemplateEntity_Id = c.Int(nullable: false),
                        ResponseTemplateEntity_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.QuestionTemplateEntity_Id, t.ResponseTemplateEntity_Id })
                .ForeignKey("dbo.QuestionsTemplates", t => t.QuestionTemplateEntity_Id, cascadeDelete: true)
                .ForeignKey("dbo.ResponsesTemplates", t => t.ResponseTemplateEntity_Id, cascadeDelete: true)
                .Index(t => t.QuestionTemplateEntity_Id)
                .Index(t => t.ResponseTemplateEntity_Id);
            
            CreateTable(
                "dbo.SurveyTemplateEntityQuestionTemplateEntities",
                c => new
                    {
                        SurveyTemplateEntity_Id = c.Int(nullable: false),
                        QuestionTemplateEntity_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.SurveyTemplateEntity_Id, t.QuestionTemplateEntity_Id })
                .ForeignKey("dbo.SurveysTemplates", t => t.SurveyTemplateEntity_Id, cascadeDelete: true)
                .ForeignKey("dbo.QuestionsTemplates", t => t.QuestionTemplateEntity_Id, cascadeDelete: true)
                .Index(t => t.SurveyTemplateEntity_Id)
                .Index(t => t.QuestionTemplateEntity_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Responses", "Survey_Id", "dbo.Surveys");
            DropForeignKey("dbo.Responses", "ResponseTemplate_Id", "dbo.ResponsesTemplates");
            DropForeignKey("dbo.SurveyTemplateEntityQuestionTemplateEntities", "QuestionTemplateEntity_Id", "dbo.QuestionsTemplates");
            DropForeignKey("dbo.SurveyTemplateEntityQuestionTemplateEntities", "SurveyTemplateEntity_Id", "dbo.SurveysTemplates");
            DropForeignKey("dbo.QuestionTemplateEntityResponseTemplateEntities", "ResponseTemplateEntity_Id", "dbo.ResponsesTemplates");
            DropForeignKey("dbo.QuestionTemplateEntityResponseTemplateEntities", "QuestionTemplateEntity_Id", "dbo.QuestionsTemplates");
            DropIndex("dbo.SurveyTemplateEntityQuestionTemplateEntities", new[] { "QuestionTemplateEntity_Id" });
            DropIndex("dbo.SurveyTemplateEntityQuestionTemplateEntities", new[] { "SurveyTemplateEntity_Id" });
            DropIndex("dbo.QuestionTemplateEntityResponseTemplateEntities", new[] { "ResponseTemplateEntity_Id" });
            DropIndex("dbo.QuestionTemplateEntityResponseTemplateEntities", new[] { "QuestionTemplateEntity_Id" });
            DropIndex("dbo.Responses", new[] { "Survey_Id" });
            DropIndex("dbo.Responses", new[] { "ResponseTemplate_Id" });
            DropTable("dbo.SurveyTemplateEntityQuestionTemplateEntities");
            DropTable("dbo.QuestionTemplateEntityResponseTemplateEntities");
            DropTable("dbo.Surveys");
            DropTable("dbo.SurveysTemplates");
            DropTable("dbo.QuestionsTemplates");
            DropTable("dbo.ResponsesTemplates");
            DropTable("dbo.Responses");
        }
    }
}
