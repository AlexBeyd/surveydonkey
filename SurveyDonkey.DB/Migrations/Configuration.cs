namespace SurveyDonkey.DB.Migrations
{
    using SurveyDonkey.DB.Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<Context.SurveyContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Context.SurveyContext context)
        {
            var questionsList = new List<QuestionTemplateEntity>()
            {
                new QuestionTemplateEntity
                {
                    ResponseTemplatesList = new List<ResponseTemplateEntity>() {
                        new ResponseTemplateEntity {
                            Content = "1"
                        },
                        new ResponseTemplateEntity {
                            Content = "2"
                        },
                        new ResponseTemplateEntity {
                            Content = "3"
                        },
                        new ResponseTemplateEntity {
                            Content = "4"
                        },
                        new ResponseTemplateEntity {
                            Content = "5"
                        },
                        new ResponseTemplateEntity {
                            Content = "6"
                        },
                        new ResponseTemplateEntity {
                            Content = "7"
                        },
                        new ResponseTemplateEntity {
                            Content = "8"
                        },
                        new ResponseTemplateEntity {
                            Content = "9"
                        },
                        new ResponseTemplateEntity {
                            Content = "10"
                        },
                    },
                    QuestionType = Common.QuestionType.Scale,
                    Content = "How likely is it that you would recommend this company to a friend or colleague?"
                },
                new QuestionTemplateEntity
                {
                    ResponseTemplatesList = new List<ResponseTemplateEntity>() {
                        new ResponseTemplateEntity {
                            Content = "Very satisfied"
                        },
                        new ResponseTemplateEntity {
                            Content = "Somewhat satisfied"
                        },
                        new ResponseTemplateEntity {
                            Content = "Neither satisfied nor dissatisfied"
                        },
                        new ResponseTemplateEntity {
                            Content = "Somewhat dissatisfied"
                        },
                        new ResponseTemplateEntity {
                            Content = "Very dissatisfied"
                        }
                    },
                    QuestionType = Common.QuestionType.SingleChoice,
                    Content = "Overall, how satisfied or dissatisfied are you with our company?"
                },
                new QuestionTemplateEntity
                {
                    ResponseTemplatesList = new List<ResponseTemplateEntity>() {
                        new ResponseTemplateEntity {
                            Content = "Reliable"
                        },
                        new ResponseTemplateEntity {
                            Content = "High Quality"
                        },
                        new ResponseTemplateEntity {
                            Content = "Useful"
                        },
                        new ResponseTemplateEntity {
                            Content = "Unique"
                        },
                        new ResponseTemplateEntity {
                            Content = "Good value for money"
                        },
                        new ResponseTemplateEntity {
                            Content = "Overpriced"
                        },
                        new ResponseTemplateEntity {
                            Content = "Impractical"
                        },
                        new ResponseTemplateEntity {
                            Content = "Ineffective"
                        },
                        new ResponseTemplateEntity {
                            Content = "Poor quality"
                        },
                        new ResponseTemplateEntity {
                            Content = "Unreliable"
                        }
                    },
                    QuestionType = Common.QuestionType.MultiChoice,
                    Content = "Which of the following words would you use to describe our products? Select all that apply."
                },
                new QuestionTemplateEntity
                {
                    ResponseTemplatesList = new List<ResponseTemplateEntity>() {
                        new ResponseTemplateEntity {
                            Content = "Extremely well"
                        },
                        new ResponseTemplateEntity {
                            Content = "Very well"
                        },
                        new ResponseTemplateEntity {
                            Content = "Somewhat well"
                        },
                        new ResponseTemplateEntity {
                            Content = "Not so well"
                        },
                        new ResponseTemplateEntity {
                            Content = "Not at all well"
                        }
                    },
                    QuestionType = Common.QuestionType.SingleChoice,
                    Content = "How well do our products meet your needs?"
                },
                new QuestionTemplateEntity
                {
                    ResponseTemplatesList = new List<ResponseTemplateEntity>() {
                        new ResponseTemplateEntity {
                            Content = "Very high quality"
                        },
                        new ResponseTemplateEntity {
                            Content = "High quality"
                        },
                        new ResponseTemplateEntity {
                            Content = "Neither hih nor low quality"
                        },
                        new ResponseTemplateEntity {
                            Content = "Low quality"
                        },
                        new ResponseTemplateEntity {
                            Content = "Very low quality"
                        }
                    },
                    QuestionType = Common.QuestionType.SingleChoice,
                    Content = "How would you rate the quality of the product?"
                },
                new QuestionTemplateEntity
                {
                    ResponseTemplatesList = new List<ResponseTemplateEntity>() {
                        new ResponseTemplateEntity {
                            Content = "Excellent"
                        },
                        new ResponseTemplateEntity {
                            Content = "Above average"
                        },
                        new ResponseTemplateEntity {
                            Content = "Average"
                        },
                        new ResponseTemplateEntity {
                            Content = "Below average"
                        },
                        new ResponseTemplateEntity {
                            Content = "Poor"
                        }
                    },
                    QuestionType = Common.QuestionType.SingleChoice,
                    Content = "How would you rate the value for money of the product?"
                },
                new QuestionTemplateEntity
                {
                    ResponseTemplatesList = new List<ResponseTemplateEntity>() {
                        new ResponseTemplateEntity {
                            Content = "Extremely responsive"
                        },
                        new ResponseTemplateEntity {
                            Content = "Very responsive"
                        },
                        new ResponseTemplateEntity {
                            Content = "Somewhat responsive"
                        },
                        new ResponseTemplateEntity {
                            Content = "Not so responsive"
                        },
                        new ResponseTemplateEntity {
                            Content = "Not at all responsive"
                        },
                        new ResponseTemplateEntity {
                            Content = "Not applicable"
                        }
                    },
                    QuestionType = Common.QuestionType.SingleChoice,
                    Content = "How responsive have we been to your questions or concerns about our products?"
                },
                new QuestionTemplateEntity
                {
                    ResponseTemplatesList = new List<ResponseTemplateEntity>() {
                        new ResponseTemplateEntity {
                            Content = "This is my first purchase"
                        },
                        new ResponseTemplateEntity {
                            Content = "Less than six months"
                        },
                        new ResponseTemplateEntity {
                            Content = "Six months to year"
                        },
                        new ResponseTemplateEntity {
                            Content = "1-2 years"
                        },
                        new ResponseTemplateEntity {
                            Content = "3 or more years"
                        },
                        new ResponseTemplateEntity {
                            Content = "I havn't made a purchase yet"
                        }
                    },
                    QuestionType = Common.QuestionType.SingleChoice,
                    Content = "How long have you been a customer of our company?"
                },
                new QuestionTemplateEntity
                {
                    ResponseTemplatesList = new List<ResponseTemplateEntity>() {
                        new ResponseTemplateEntity {
                            Content = "Extremely likely"
                        },
                        new ResponseTemplateEntity {
                            Content = "Very likely"
                        },
                        new ResponseTemplateEntity {
                            Content = "Somewhat likely"
                        },
                        new ResponseTemplateEntity {
                            Content = "Not so likely"
                        },
                        new ResponseTemplateEntity {
                            Content = "Not at all likely"
                        }
                    },
                    QuestionType = Common.QuestionType.SingleChoice,
                    Content = "How likely are you to purchase any of our products again?"
                },
                new QuestionTemplateEntity
                {
                    ResponseTemplatesList = new List<ResponseTemplateEntity>() {
                        new ResponseTemplateEntity {
                            Content = ""
                        }
                    },
                    QuestionType = Common.QuestionType.FreeText,
                    Content = "Do you have any other comments, questions, or concerns?"
                }

            };

            var survey = new SurveyTemplateEntity
            {
                QuestionTemplatesList = questionsList,
                CreatedTimestamp = DateTime.Now,
                SurveyName = "Customer Satisfaction Survey"
            };

            context.SurveyTemplates.Add(survey);
            context.SaveChanges();
        }
    }
}
