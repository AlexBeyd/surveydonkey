﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SurveyDonkey.DB;
using System.Threading.Tasks;
using SurveyDonkey.DB.Models;

namespace SurveyDonkey.Tests
{
    /// <summary>
    /// Summary description for Db
    /// </summary>
    [TestClass]
    public class DbIntegrationTest
    {
        private SurveyDb _repoUnderTest;
        private Guid _newSurveyId;

        [TestInitialize]
        public void Init()
        {
            _repoUnderTest = new SurveyDb();
            _newSurveyId = Guid.NewGuid();
        }

        [TestCleanup]
        public void MyTestCleanup() {

        }

        [TestMethod]
        public async Task MustHaveAtLeastOneTemplate()
        {
            var surveys = await _repoUnderTest.GetSurveyTemplatesList();
            Assert.IsTrue(surveys.Count >= 1);
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
    }
}
